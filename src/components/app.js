import React, { Component } from 'react';
import { connect } from 'react-redux';

import { addTodo } from '../redux/actions';

class App extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <h2>My Minimal React/Redux/Webpack/Babel Setup</h2>
      </div>
    )
  }
}


export default connect(
  null,
  { addTodo }
)(App);
